
extern "C" {
    fn _aligned_malloc(size: usize, alignment: usize) -> *mut u8;

    #[link_name = "_aligned_free"]
    pub fn aligned_free(p: *mut u8);
}

pub unsafe fn aligned_alloc(alignment: usize, size: usize) -> *mut u8
{
    _aligned_malloc(size, alignment)
}
