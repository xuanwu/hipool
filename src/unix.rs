
extern "C" {
    pub fn aligned_alloc(alignment: usize, size: usize) -> *mut u8;

    #[link_name = "free"]
    pub fn aligned_free(p: *mut u8);
}
