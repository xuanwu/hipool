//!
//! 提供内存池管理机制, 支持从堆上动态分配，或者事先分配的一块内存空间，可能在堆上也可能在栈上.
//!
//! # Examples
//!
//! 简单类型，直接传递初始值，会有一次拷贝
//! ```rust
//! use hipool::{ MemPool, Boxed};
//! let pool = MemPool::new(0);
//! let psize = Boxed::new_in(&pool, 100).unwrap();
//! assert_eq!(*psize, 100);
//! ```
//!
//! 复杂类型，构造过程中还会涉及动态申请内存.
//! ```rust
//! use core::ptr;
//! use hipool::{ MemPool, Boxed};
//!
//! let pool = MemPool::new(0);
//! struct Foo<'a> {
//!     val: Boxed<'a, i32, MemPool>,
//! }
//! let foo = Boxed::new_then_in(&pool, || {
//!     Ok(Foo {
//!         val: Boxed::new_in(&pool, 100)?,
//!     })
//! }).unwrap();
//! assert_eq!(*foo.val, 100);
//!
//! // 也可以如下申请，两种申请方式，在内存池中内存的申请顺序不同
//! let val = Boxed::new_in(&pool, 99).unwrap();
//! let foo = Boxed::new_in(&pool, Foo { val }).unwrap();
//! assert_eq!(*foo.val, 99);
//! ```
//!
//! 有时候需要在栈上分配数据，需要把栈空间适配为Pool接口，则可使用BufPool完成适配
//! ```rust
//! use hipool::{ BufPool, Boxed};
//! let mut buf = [0_u8; 100];
//! let mut pool = BufPool::new(&mut buf);
//! let psize = Boxed::new_in(&pool, 100).unwrap();
//! assert_eq!(*psize, 100);
//! ```
//!
//! 有时候分配的Pool也需要在堆上，可以用另外的构造方式，如下:
//!
//! ```rust
//! use hipool::{ MemPool, Boxed };
//!
//! let mut pool = MemPool::new_boxed(0).unwrap();
//! let int_array = Boxed::new_slice_then_in(&*pool, 100, |_| {
//!     Ok(0)
//! }).unwrap();
//! int_array.iter().for_each(|n| assert_eq!(*n, 0));
//! ```
//!
//! 更多的调用接口
//!
//! ```rust
//! use hipool::{ MemPool, Boxed };
//! use core::alloc::Layout;
//!
//! let pool = MemPool::new_boxed(0).unwrap();
//!
//! let val = Boxed::new_in(&*pool, 1).unwrap();
//! assert_eq!(*val, 1);
//!
//! let val = Boxed::new_then_in(&*pool, || {
//!     Ok(100)
//! }).unwrap();
//! assert_eq!(*val, 100);
//!
//! let ivals = [0, 1, 2, 3];
//! let vals = Boxed::uninit_slice_in::<i32>(&*pool, ivals.len()).unwrap();
//! let vals = vals.write_slice_then(|n| {Ok(ivals[n])}).unwrap();
//! assert_eq!(vals[0], 0);
//! assert_eq!(vals[1], 1);
//! assert_eq!(vals[2], 2);
//! assert_eq!(vals[3], 3);
//!
//! let vals = unsafe { Boxed::new_buf_in(&*pool,
//! Layout::array::<i32>(2).unwrap()).unwrap().cast_slice::<i32>(2) };
//! vals.iter().for_each(|val| println!(" {:?} ", val));
//!
//! let vals = Boxed::new_slice_then_in(&*pool, 2, |n| {
//!     Ok(n as i32)
//! }).unwrap();
//! assert_eq!(vals[0], 0);
//! assert_eq!(vals[1], 1);
//! ```
//!

#![no_std]

pub use hierr::{Error, Result};

mod boxed;
mod buf;
mod mem;
pub(crate) mod util;

mod alloc;
pub use crate::boxed::Boxed;
pub use crate::buf::BoxedPool as BoxedBufPool;
pub use crate::buf::Pool as BufPool;
pub use crate::mem::BoxedPool as BoxedMemPool;
pub use crate::mem::Pool as MemPool;
pub use alloc::*;

mod rc;
pub use rc::*;
mod arc;
pub use arc::*;

cfg_if::cfg_if! {
    if #[cfg(windows)] {
        mod windows;
        use windows as stdlib;
    } else if #[cfg(unix)] {
        mod unix;
        use unix as stdlib;
    }
}

#[cfg(test)]
mod test {
    use crate::{Boxed, BufPool, MemPool};

    struct Foo {
        val: usize,
    }

    static mut DROP_CNT: usize = 0;

    impl Drop for Foo {
        fn drop(&mut self) {
            unsafe { DROP_CNT += self.val };
        }
    }

    #[test]
    fn test_mem_drop() {
        let pool = MemPool::new(0);
        unsafe {
            DROP_CNT = 0;
        }

        {
            let _ = Boxed::new_in(&pool, Foo { val: 100 });
        }

        unsafe {
            assert_eq!(100, DROP_CNT);
        }
    }
    #[test]
    fn test_buf_drop() {
        let mut buf = [0_u8; 100];
        let pool = BufPool::new(&mut buf);
        unsafe {
            DROP_CNT = 0;
        }

        {
            let _ = Boxed::new_in(&pool, Foo { val: 100 });
        }

        unsafe {
            assert_eq!(100, DROP_CNT);
        }
    }

    #[test]
    fn test_static() {
        let pool = MemPool::new_boxed(0).unwrap();
        let x = Boxed::new_in(&pool, 100).unwrap();
        assert_eq!(*x, 100);
    }

    #[test]
    fn test_struct_pool() {
        let pool = MemPool::new_boxed(0).unwrap();
        let _v32 = Boxed::new_in(&pool, 100);
    }
}
